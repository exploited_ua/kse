//= ../../node_modules/jquery/dist/jquery.js
//= ../../node_modules/swiper/dist/js/swiper.js
//= ../../node_modules/nouislider/distribute/nouislider.js
//= ../../node_modules/simplebar/dist/simplebar.js
//= ../../node_modules/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js


$(document).ready(function () {
    var swiper = new Swiper('.profesor-info', {
        pagination: {
            el: '.swiper-pagination',
            clickable: true
        }
    });
    var projectswiper = new Swiper('.slider-project', {
        slidesPerView: 2,
        loop: 'true',
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev'
        }
    });



    $('ul.tabs1 li').click(function () {
        var tab_id = $(this).attr('data-tab');

        $('ul.tabs1 li').removeClass('current');
        $('.tab-content1').removeClass('current');

        $(this).addClass('current');
        $("#" + tab_id).addClass('current');
    });

    $('ul.tabs2 li').click(function () {
        var tab_id = $(this).attr('data-tab');

        $('ul.tabs2 li').removeClass('current');
        $('.tab-content2').removeClass('current');

        $(this).addClass('current');
        $("#" + tab_id).addClass('current');
    });

    $('ul.tabs3 li').click(function () {
        var tab_id = $(this).attr('data-tab');

        $('ul.tabs3 li').removeClass('current');
        $('.tab-content3').removeClass('current');

        $(this).addClass('current');
        $("#" + tab_id).addClass('current');
    });
    var dateSlider = document.getElementById('slider-date');

    if(dateSlider){
    noUiSlider.create(dateSlider, {
        range: {
            min: 1,
            max: 5
        },
        connect: true,
        pips: {
            mode: 'count',
            values: 5,
            density: 100
        },
        step: 1,

        start: [1, 4]


    });

    $('.noUi-value.noUi-value-horizontal.noUi-value-large').each(function () {
        var val = $(this).html();
        val = recountVal(parseInt(val));
        $(this).html(val);
    });


    function recountVal(val) {
        switch (val) {
            case 1:
                return '2d';
                break;
            case 2:
                return '2w';
                break;
            case 3:
                return '3m';
                break;
            case 4:
                return '1y';
                break;
            case 5:
                return '2y';
                break;
            default :
                return '1y';
                break;
        }
    }

}


});

$('#datepicker').datepicker({
    multidate:true,

});
$('#datepicker').on('changeDate', function() {
    $('#my_hidden_input').val(
        $('#datepicker').datepicker('getFormattedDate')
    );


});

$('.toggle-menu').on('click', function(e) {
    e.preventDefault();
    $('.header-nav, .header-nav-blue').toggleClass('open');
    $('body').toggleClass('mobile-menu-open');
});

$(".close-btn").on("click", function (e) {
    e.preventDefault();
    $('.header-nav, .header-nav-blue').removeClass('open');
    $('body').removeClass('mobile-menu-open');
});
